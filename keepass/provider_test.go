package keepass

import (
	"github.com/hashicorp/terraform/terraform"
	"github.com/hashicorp/terraform/helper/schema"
	"math/rand"
	"time"
)

var testAccProvider *schema.Provider
var testAccProviders map[string]terraform.ResourceProvider

func init() {
	rand.Seed(time.Now().Unix())

	testAccProvider = Provider().(*schema.Provider)
	testAccProviders = map[string]terraform.ResourceProvider{
		"keepass": testAccProvider,
	}
}

// var testAccProviders = map[string]terraform.ResourceProvider{
// 	"keepass": Provider(),
// }
