package keepass

import (
	"github.com/hashicorp/terraform/helper/schema"
	"os"
	"github.com/tobischo/gokeepasslib"
	"github.com/hashicorp/terraform/terraform"
)

func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"file": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Location of .kdbx file",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Password for .kdbx file",
			},
		},
		ConfigureFunc: providerConfigure,

		DataSourcesMap: map[string]*schema.Resource{
			"keepass_value": resourceKeepassValue(),
		},
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {

	fileLocation := d.Get("file").(string)
	password := d.Get("password").(string)

	file, err := os.Open(fileLocation)

	if err != nil {
		return nil, err
	}

	db := gokeepasslib.NewDatabase()
	db.Credentials = gokeepasslib.NewPasswordCredentials(password)
	err = gokeepasslib.NewDecoder(file).Decode(db)

	if err != nil {
		return nil, err
	}

	err = db.UnlockProtectedEntries()

	if err != nil {
		return nil, err
	}

	return db, nil
}